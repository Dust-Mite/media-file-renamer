﻿using MySql.Data.MySqlClient;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;

namespace Media_File_Renamer_CMD
{
    class Program
    {

        myVariables myVariables = new myVariables();
        extraMain extraMain = new extraMain();

        static void Main(string[] args)
        {
            new Program().main2(args);
        }

        private void main2(string[] args)
        {
            Console.WriteLine("Got to ");

            StreamReader readerIgnoreEpisodes = new StreamReader(@"Data/ShowIgnore.txt");
            string listerIgnoreEpisodes = readerIgnoreEpisodes.ReadToEnd();
            listerIgnoreEpisodes = extraMain.replacer(listerIgnoreEpisodes, "\r\n", "");
            myVariables.ignoreEpisodes = listerIgnoreEpisodes.Split('|');

            readerIgnoreEpisodes.Close();

            Console.WriteLine("Got to first");

            myVariables.extensions = ".mp4|.avi|.mkv|.wmv|.flv|.mpg|.mov|.rmvb|.TS";

            string[] directories = args[0].Split('|');
            myVariables.folderName = args[1];

            StreamReader readerID = new StreamReader(@"Data/ids.txt");
            string listerTmpID = readerID.ReadToEnd();
            listerTmpID = extraMain.replacer(listerTmpID, "\r\n", "");
            string[] multiIgnoreID = listerTmpID.Split('|');

            readerID.Close();

            Console.WriteLine("Got to second");

            int dirs = 0;

            foreach (string dir in directories)
            {
                string[] seasons = Directory.GetDirectories(dir);

                foreach (string sea in seasons)
                {
                    dirs++;
                }

            }

            myVariables.ids = new string[dirs + 100, 2];

            int IDI = 0;
            foreach(string IDS in multiIgnoreID)
            {
                string[] temp = IDS.Split('=');

                myVariables.ids[IDI, 0] = temp[0];
                myVariables.ids[IDI, 1] = temp[1];

                IDI++;
            }

            foreach (string dir in directories)
            {

                string[] seasons = Directory.GetDirectories(dir);

                StreamReader reader = new StreamReader(@"Data/ignore.txt");
                string listerTmp = reader.ReadToEnd();
                string[] multiIgnore = listerTmp.Split('|');
                reader.Close();

                for (int i = 0; i < seasons.Length; i++) //Cleans the show names
                {
                    seasons[i] = extraMain.replacer(seasons[i], dir + "|" + "\\", "");
                    seasons[i] = extraMain.replacer(seasons[i], " ", "_");
                    seasons[i] = extraMain.replacer(seasons[i], "/", "");
                    //Console.WriteLine(seasons[i]);
                }

                foreach (string season in seasons)
                {
                    myVariables.showName = "";
                    myVariables.showXML = "";
                    myVariables.folders = null;
                    myVariables.files = null;
                    myVariables.season = 0;
                    myVariables.episode = 0;
                    myVariables.ignoreName = "";

                    bool ignored = false;
                    bool done = false;
                    foreach (string ignore in multiIgnore)
                    {
                        if (season == ignore && ignored == false) // Checks if show is on ignore list
                        {
                            ignored = true;
                        }
                    }

                    if (ignored == false)
                    {
                        myVariables.id = -1;
                        done = false;

                        for (int jj = 0; jj < (myVariables.ids.Length / 2); jj++)
                        {
                            if (myVariables.ids[jj, 0] == season)
                            {
                                myVariables.id = int.Parse(myVariables.ids[jj, 1]);

                                done = true;
                            }
                        }

                        if (done == false)
                        {
                            string code = extraMain.pageDownloader("http://thetvdb.com/api/GetSeries.php?seriesname=" + season, "<Series>", "</Series>"); // Finds and gets the xml of each show
                            myVariables.id = int.Parse(extraMain.xmlLoader(code, "<seriesid>", "</seriesid>"));

                            System.IO.File.AppendAllText("Data/ids.txt", "|" + season + "=" + myVariables.id); // Updates id file
                        }

                        //myVariables.showXML = extraMain.pageDownloader(("http://www.thetvdb.com/api/A2ABD84CB07FA632/series/" + myVariables.id + "/all"), "<Data>", "</Data>");
                        myVariables.showXML = File.ReadAllText("XML Files/" + myVariables.id + ".xml");

                        string seasonH = season.Replace("_", " ");
                        string[] folderHolder = Directory.GetDirectories(dir + "/" + seasonH); // Gets each season folder
                        //Console.WriteLine(folderHolder[4]);

                        folderDeletion(folderHolder); // deletes unneeded folders

                        //extraMain.numericalSort(folderHolder);
                        seriesSorter(folderHolder);
                        //episodeSorter();

                        string[] seriesholder = seasonH.Split('/'); // Cleans series name
                        myVariables.showName = seriesholder[seriesholder.Length - 1];

                        for (int i = 0; i < (myVariables.folders.Length / 2); i++) // Cycles through each season
                        {
                            episodeSorter();
                            //Console.WriteLine(seasonH + " is " + (myVariables.folders.Length / 2));
                            for (int j = 0; j < (myVariables.files.Length / 3); j++) // Cycles through each file
                            {
                                tvdbNamer(); // Gets file name from xml
                                myVariables.fullName = myVariables.folders[myVariables.season, 0] + "/" + myVariables.showName + " - s" + myVariables.folders[myVariables.season, 1] + "e" + myVariables.files[myVariables.episode, 1] + " - " + myVariables.episodeName;
                                myVariables.ignoreName = myVariables.showName + " - s" + myVariables.folders[myVariables.season, 1] + "e" + myVariables.files[myVariables.episode, 1];
                                Console.WriteLine(myVariables.showName + " - s" + myVariables.folders[myVariables.season, 1] + "e" + myVariables.files[myVariables.episode, 1] + " - " + myVariables.episodeName);
                                myVariables.episode++; // Goes to next file
                                save(); // Saves file name
                            }
                            myVariables.episode = 0;

                            if (myVariables.folders[myVariables.season, 1] != "00") // Checks if specials season
                            {
                                myVariables.season++;
                            }
                            else
                            {
                                myVariables.season = 0;
                            }
                        }
                    }
                }
            }

        }

        private void folderDeletion(string[] folderHolder)
        {
            foreach (string folder in folderHolder)
            {
                string[] folderHolderMove = Directory.GetDirectories(folder);

                foreach (string folderMove in folderHolderMove)
                {
                    //Console.WriteLine(folderMove);
                    string[] fileHolder = extraMain.getFiles(folderMove, myVariables.extensions, SearchOption.AllDirectories);

                    foreach (string filei in fileHolder)
                    {
                        //Console.WriteLine(file);

                        try
                        {
                            string temp = extraMain.replacer(filei, folderMove + "|\\|//", "");
                            string temp2 = filei.Replace(temp, "");

                            Console.WriteLine(temp2);
                            Console.WriteLine(filei);

                            File.Move(filei, folder + "/" + temp);
                            Directory.Delete(temp2, true);

                        }
                        catch (FileNotFoundException)
                        {

                        }
                        catch (ArgumentException)
                        {

                        }
                        catch (NotSupportedException)
                        {

                        }
                        catch (IOException)
                        {

                        }
                    }

                }
            }
        }

        private void save()
        {
            bool found = true;

            foreach (string ignore in myVariables.ignoreEpisodes)
            {
                if (myVariables.ignoreName == ignore)
                {
                    found = false;
                }
            }

            if (found == true)
            {
                saveAfter();
            }

        }

        private void saveAfter()
        {
            try
            {
                FileInfo file = new FileInfo(myVariables.fullName);

                string[] MultipleFilters = myVariables.extensions.Split('|'); // Gets all extensions into array

                foreach (string extension in MultipleFilters)
                {
                    if (myVariables.files[myVariables.episode - 1, 2].EndsWith(extension))
                    {
                        if ((file + extension) != myVariables.files[myVariables.episode - 1, 2])
                        {
                            //Console.WriteLine(@myVariables.files[myVariables.episode - 1, 2]);
                            //Console.WriteLine(file + extension);
                            //Console.ReadLine();

                            File.Move(@myVariables.files[myVariables.episode - 1, 2], file + extension); // Renames file
                            dataBaseSaver((@myVariables.files[myVariables.episode - 1, 2]), ((file + extension) + "\r\n"));
                            System.IO.File.AppendAllText("Data/log.txt", "Renamed: " + @myVariables.files[myVariables.episode - 1, 2] + " to " + (file + extension) + "\r\n"); // Updates log file
                            Console.WriteLine("Renamed: " + @myVariables.files[myVariables.episode - 1, 2] + " to " + (file + extension));
                        }
                    }
                }
            }
            catch (FileNotFoundException)
            {

            }
            catch (ArgumentException)
            {

            }
            catch (NotSupportedException)
            {

            }
            catch (IOException)
            {

            }
        }

        private void dataBaseSaver(string from, string to)
        {
            from = from.Replace(@"\", @"\\");
            from = from.Replace(@"'", @"\'");

            to = to.Replace(@"\", @"\\");
            to = to.Replace(@"'", @"\'");

            //System.IO.File.AppendAllText("log2.txt", "Renamed: " + from + " to " + to); // Updates log file

            //String str = Media_File_Renamer_CMD.Properties.Settings.Default.databaseConnectionString.ToString();
            String str = @"server=localhost;database=media_log;userid=Media_Log;password=aMVNGdjKVVQA3vW5";

            DateTime temp = DateTime.Now;
            int year = temp.Year;
            int month = temp.Month;
            int date = temp.Day;
            int hour = temp.Hour;
            int min = temp.Minute;
            int sec = temp.Second;

            String dateS = year + "-" + month + "-" + date + " " + hour + ":" + min + ":" + sec;

            MySqlConnection con = null;
            //MySqlDataReader Object
            MySqlDataReader reader = null;
            try
            {
                con = new MySqlConnection(str);
                con.Open(); //open the connection
                //We will need to SELECT all or some columns in the table
                //via this command
                String cmdText = "INSERT INTO `log` (`FromCol`, `ToCol`, `Date`) VALUES ( '" + from + "', '" + to + "', '" + dateS + "')";
                MySqlCommand cmd = new MySqlCommand(cmdText, con);
                reader = cmd.ExecuteReader(); //execure the reader
                /*The Read() method points to the next record It return false if there are no more records else returns true.*/

            }
            catch (MySqlException err)
            {
                //Console.WriteLine("Error: " + err.ToString());
                //MessageBox.Show("Error: " + err.ToString());
            }
            finally
            {
                if (reader != null)
                {
                    reader.Close();
                }
                if (con != null)
                {
                    con.Close(); //close the connection
                }
            } //remember to close the connection after accessing the database
        }

        private void seriesSorter(string[] folderHolder)
        {
            myVariables.folders = new string[(folderHolder.Length), 2];

            try
            {
                for (int i = 0; i < folderHolder.Length; i++)
                {
                    string[] holder = folderHolder[i].Split('/');
                    string[] holderNum = holder[holder.Length - 1].Split(' '); // Gets season number from folder name
                    string address = folderHolder[i];
                    string number = "00";

                    if (holderNum[holderNum.Length - 1] != "Specials") // Checks to make sure folder is not Specials
                    {
                        if ((Convert.ToInt32(holderNum[holderNum.Length - 1])) > 9)
                        {
                            number = holderNum[holderNum.Length - 1];
                        }
                        else
                        {
                            number = "0" + holderNum[holderNum.Length - 1];
                        }
                    }
                    else
                    {
                        number = "00"; // Sets number as 00 if folder name is Specials
                    }

                    myVariables.folders[i, 0] = folderHolder[i];
                    myVariables.folders[i, 1] = number;
                }
            }
            catch
            {
                
            }
        }

        private void episodeSorter()
        {
            string[] fileholder = extraMain.getFiles(myVariables.folders[myVariables.season, 0], myVariables.extensions, SearchOption.AllDirectories); // Gets all files from folder
            //Console.WriteLine(fileholder.Length);
            List<string> multiplier = new List<string>();
            multiplier.Add("-1");

            myVariables.files = new string[(fileholder.Length), 3];
            Regex rgx = new Regex(@"(e|E|x|X)([\d][\d][\d]|[\d][\d])");

            string episodeNum = "";

            for (int i = 0; i < fileholder.Length; i++)
            {
                int j = 0;
                foreach (Match match in rgx.Matches(fileholder[i].Replace(myVariables.folderName, "")))
                {
                    if (j == 0)
                    {
                        //Console.WriteLine(match.Value);
                        //Console.WriteLine(fileholder[i]);
                        //Console.WriteLine(extraMain.replacer(episodeNum, "e|E|x|X", ""));
                        //Console.ReadLine();
                        string num = fileholder[i].Substring(match.Index - 1, 2);
                        episodeNum = (match.Value + "");
                        episodeNum = extraMain.replacer(episodeNum, "e|E|x|X", ""); // Deletes any episode prefix from the number
                    }
                    j++;
                }
                myVariables.files[i, 0] = (extraMain.replacer(fileholder[i], myVariables.extensions + "|" + myVariables.folders[myVariables.season, 0] + "|" + "\\", ""));
                myVariables.files[i, 0] = (extraMain.replacer(myVariables.files[i, 0], "/", ""));
                myVariables.files[i, 1] = episodeNum;
                
                myVariables.files[i, 2] = fileholder[i]; // Updates file array
                //Console.WriteLine(myVariables.files[i, 0]);

                for (int o = 0; o < multiplier.Count; o++ )
                {
                    if (episodeNum == multiplier[o])
                    {
                        System.IO.File.AppendAllText("Data/duplicate.txt", "Duplicate found at: " + fileholder[i] + "\r\n"); // Updates log file
                    }
                }

                multiplier.Add(episodeNum);
            }
        }

        private void tvdbNamer()
        {
            string seasonNum = myVariables.folders[myVariables.season, 1];
            string episodeNum = myVariables.files[myVariables.episode, 1];

            if (Convert.ToInt32(seasonNum) < 10 && Convert.ToInt32(seasonNum) != 00)
            {
                seasonNum = seasonNum.Replace("0", "");
            }
            else if (Convert.ToInt32(seasonNum) == 00)
            {
                seasonNum = "0";
            }


            if (Convert.ToInt32(episodeNum) < 10)
            {
                episodeNum = episodeNum.Replace("0", "");
            }

            //string xml = extraMain.xmlLoader(myVariables.showXML, "<Data>", "</Data>");

            string[] episodes = myVariables.showXML.Split(new string[] { "<Episode>" }, StringSplitOptions.None);

            string name = "";

            foreach (string episode in episodes)
            {
                if (episode.Contains("<EpisodeNumber>" + episodeNum + "</EpisodeNumber>") && episode.Contains("<SeasonNumber>" + seasonNum + "</SeasonNumber>"))
                {
                    name = extraMain.xmlLoader(episode, "<EpisodeName>", "</EpisodeName>");
                }
            }

            if (name != null)
            {
                name = extraMain.replacer(name, "\\|/|?|:|*|\"|>|<|amp;|&quot;", "");
                //name = extraMain.replacer(name, " &|&|& ", " and");
            }

            myVariables.episodeName = name.Trim();
        }
    }

    public class myVariables
    {
        public string folderName { get; set; }
        public string extensions { get; set; }
        public string showName { get; set; }
        public string showXML { get; set; }
        public string episodeName { get; set; }
        public string fullName { get; set; }
        public string ignoreName { get; set; }
        public string[,] folders { get; set; }
        public string[,] files { get; set; }
        public string[,] ids { get; set; }
        public string[] ignoreEpisodes { get; set; }
        public int season { get; set; }
        public int episode { get; set; }
        public int id { get; set; }
    }
}
