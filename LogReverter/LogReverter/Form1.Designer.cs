﻿namespace LogReverter
{
    partial class Form1
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.lvLog = new System.Windows.Forms.ListView();
            this.chFrom = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.chTo = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.chDate = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.chReverted = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.chID = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.cmdRevert = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // lvLog
            // 
            this.lvLog.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.lvLog.Columns.AddRange(new System.Windows.Forms.ColumnHeader[] {
            this.chID,
            this.chFrom,
            this.chTo,
            this.chDate,
            this.chReverted});
            this.lvLog.FullRowSelect = true;
            this.lvLog.LabelWrap = false;
            this.lvLog.Location = new System.Drawing.Point(12, 12);
            this.lvLog.Name = "lvLog";
            this.lvLog.Size = new System.Drawing.Size(1384, 384);
            this.lvLog.TabIndex = 0;
            this.lvLog.UseCompatibleStateImageBehavior = false;
            this.lvLog.View = System.Windows.Forms.View.Details;
            this.lvLog.ColumnClick += new System.Windows.Forms.ColumnClickEventHandler(this.lvLog_ColumnClick);
            this.lvLog.SelectedIndexChanged += new System.EventHandler(this.lvLog_SelectedIndexChanged);
            // 
            // chFrom
            // 
            this.chFrom.DisplayIndex = 0;
            this.chFrom.Text = "From";
            this.chFrom.Width = 418;
            // 
            // chTo
            // 
            this.chTo.DisplayIndex = 1;
            this.chTo.Text = "To";
            this.chTo.Width = 488;
            // 
            // chDate
            // 
            this.chDate.DisplayIndex = 2;
            this.chDate.Text = "Date";
            this.chDate.Width = 106;
            // 
            // chReverted
            // 
            this.chReverted.DisplayIndex = 3;
            this.chReverted.Text = "Reverted?";
            // 
            // chID
            // 
            this.chID.DisplayIndex = 4;
            this.chID.Text = "ID";
            // 
            // cmdRevert
            // 
            this.cmdRevert.Enabled = false;
            this.cmdRevert.Location = new System.Drawing.Point(1402, 12);
            this.cmdRevert.Name = "cmdRevert";
            this.cmdRevert.Size = new System.Drawing.Size(75, 23);
            this.cmdRevert.TabIndex = 1;
            this.cmdRevert.Text = "Revert";
            this.cmdRevert.UseVisualStyleBackColor = true;
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1489, 476);
            this.Controls.Add(this.cmdRevert);
            this.Controls.Add(this.lvLog);
            this.Name = "Form1";
            this.Text = "Form1";
            this.Load += new System.EventHandler(this.Form1_Load);
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.ListView lvLog;
        private System.Windows.Forms.ColumnHeader chFrom;
        private System.Windows.Forms.ColumnHeader chTo;
        private System.Windows.Forms.ColumnHeader chDate;
        private System.Windows.Forms.ColumnHeader chReverted;
        private System.Windows.Forms.ColumnHeader chID;
        private System.Windows.Forms.Button cmdRevert;
    }
}

