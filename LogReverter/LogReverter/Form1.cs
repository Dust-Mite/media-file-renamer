﻿using MySql.Data.MySqlClient;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace LogReverter
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
        }

        private int sortColumn = -1;

        private void Form1_Load(object sender, EventArgs e)
        {
            databaseLoad();

            sortColumn = 0;
            lvLog.Sorting = SortOrder.Descending;
            lvLog.ListViewItemSorter = new listViewItemComparer(0, lvLog.Sorting);
            lvLog.Sort();

        }

        void databaseLoad()
        {
            //String str = VEPS.Properties.Settings.Default.databaseConnectionString.ToString();
            String str = @"server=192.168.0.10;database=media_log;userid=Media_Log;password=aMVNGdjKVVQA3vW5";

            MySqlConnection con = null;
            //MySqlDataReader Object
            MySqlDataReader reader = null;
            try
            {
                con = new MySqlConnection(str);
                con.Open(); //open the connection
                //We will need to SELECT all or some columns in the table
                //via this command
                String cmdText = "SELECT * FROM log";
                MySqlCommand cmd = new MySqlCommand(cmdText, con);
                reader = cmd.ExecuteReader(); //execure the reader
                /*The Read() method points to the next record It return false if there are no more records else returns true.*/
                while (reader.Read())
                {
                        ListViewItem LVI = new ListViewItem(reader.GetString(0));

                        LVI.SubItems.Add(String.Format(reader.GetString(1)));
                        LVI.SubItems.Add(String.Format(reader.GetString(2)));
                        LVI.SubItems.Add(String.Format(reader.GetString(4))); // Difference
                        LVI.SubItems.Add(String.Format(reader.GetString(3))); // Margin

                    lvLog.Items.Add(LVI);

                    Application.DoEvents();

                }
            }
            catch (MySqlException err)
            {
                //Console.WriteLine("Error: " + err.ToString());
                MessageBox.Show("Error: " + err.ToString());
            }
            finally
            {
                if (reader != null)
                {
                    reader.Close();
                }
                if (con != null)
                {
                    con.Close(); //close the connection
                }
            } //remember to close the connection after accessing the database
        }

        private void lvLog_SelectedIndexChanged(object sender, EventArgs e)
        {
            cmdRevert.Enabled = true;
        }

        private void lvLog_ColumnClick(object sender, System.Windows.Forms.ColumnClickEventArgs e)
        {
            if (e.Column != sortColumn)
            {
                // Set the sort column to the new column.
                //sortColumn = e.Column;
                // Set the sort order to ascending by default.
                //lvLog.Sorting = SortOrder.Descending;
            }
            else if (e.Column == 0)
            {
                // Determine what the last sort order was and change it.
                if (lvLog.Sorting == SortOrder.Ascending)
                    lvLog.Sorting = SortOrder.Descending;
                else
                    lvLog.Sorting = SortOrder.Ascending;
            }

            if (e.Column == 0)
            {
                lvLog.ListViewItemSorter = new listViewItemComparer(e.Column, lvLog.Sorting);
                lvLog.Sort();
            }
        }
    }
}
