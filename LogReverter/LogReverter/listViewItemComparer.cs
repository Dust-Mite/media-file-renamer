﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace LogReverter
{
    class listViewItemComparer : IComparer
    {

        private int _colIndex;
        private SortOrder order;
        public listViewItemComparer(int colIndex, SortOrder order)
        {
            _colIndex = colIndex;
            this.order = order;
        }
        public int Compare(object x, object y)
        {
            int returnVal = -1;

            double nx = double.Parse((x as ListViewItem).SubItems[_colIndex].Text);
            double ny = double.Parse((y as ListViewItem).SubItems[_colIndex].Text);

            returnVal = nx.CompareTo(ny);

            if (order == SortOrder.Descending)
                returnVal *= -1;

            return returnVal;
        }
    }
}
