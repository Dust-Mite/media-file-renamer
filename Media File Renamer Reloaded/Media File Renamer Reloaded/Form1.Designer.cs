﻿namespace Media_File_Renamer_Reloaded
{
    partial class Form1
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.txtAddress = new System.Windows.Forms.TextBox();
            this.cmdBrowse = new System.Windows.Forms.Button();
            this.folderBrowserDialog1 = new System.Windows.Forms.FolderBrowserDialog();
            this.txtSeason = new System.Windows.Forms.TextBox();
            this.txtEpisode = new System.Windows.Forms.TextBox();
            this.txtSeasonNum = new System.Windows.Forms.TextBox();
            this.txtEpisodeNum = new System.Windows.Forms.TextBox();
            this.cmdPreviousSeason = new System.Windows.Forms.Button();
            this.cmdNextEpisode = new System.Windows.Forms.Button();
            this.cmdNextSeason = new System.Windows.Forms.Button();
            this.cmdPreviousEpisode = new System.Windows.Forms.Button();
            this.txtNameOnly = new System.Windows.Forms.TextBox();
            this.txtTVDbName = new System.Windows.Forms.TextBox();
            this.txtFullAddress = new System.Windows.Forms.TextBox();
            this.cmdUseOwn = new System.Windows.Forms.Button();
            this.cmdUseTVDB = new System.Windows.Forms.Button();
            this.txtFullAddressBefore = new System.Windows.Forms.TextBox();
            this.cmdSave = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // txtAddress
            // 
            this.txtAddress.Location = new System.Drawing.Point(12, 12);
            this.txtAddress.Name = "txtAddress";
            this.txtAddress.Size = new System.Drawing.Size(545, 20);
            this.txtAddress.TabIndex = 0;
            // 
            // cmdBrowse
            // 
            this.cmdBrowse.Location = new System.Drawing.Point(563, 10);
            this.cmdBrowse.Name = "cmdBrowse";
            this.cmdBrowse.Size = new System.Drawing.Size(125, 23);
            this.cmdBrowse.TabIndex = 1;
            this.cmdBrowse.Text = "Browse";
            this.cmdBrowse.UseVisualStyleBackColor = true;
            this.cmdBrowse.Click += new System.EventHandler(this.cmdBrowse_Click);
            // 
            // txtSeason
            // 
            this.txtSeason.Location = new System.Drawing.Point(12, 38);
            this.txtSeason.Name = "txtSeason";
            this.txtSeason.Size = new System.Drawing.Size(545, 20);
            this.txtSeason.TabIndex = 2;
            // 
            // txtEpisode
            // 
            this.txtEpisode.Location = new System.Drawing.Point(12, 64);
            this.txtEpisode.Name = "txtEpisode";
            this.txtEpisode.Size = new System.Drawing.Size(545, 20);
            this.txtEpisode.TabIndex = 3;
            // 
            // txtSeasonNum
            // 
            this.txtSeasonNum.Location = new System.Drawing.Point(12, 110);
            this.txtSeasonNum.Name = "txtSeasonNum";
            this.txtSeasonNum.Size = new System.Drawing.Size(100, 20);
            this.txtSeasonNum.TabIndex = 4;
            // 
            // txtEpisodeNum
            // 
            this.txtEpisodeNum.Location = new System.Drawing.Point(12, 136);
            this.txtEpisodeNum.Name = "txtEpisodeNum";
            this.txtEpisodeNum.Size = new System.Drawing.Size(100, 20);
            this.txtEpisodeNum.TabIndex = 5;
            // 
            // cmdPreviousSeason
            // 
            this.cmdPreviousSeason.Location = new System.Drawing.Point(118, 108);
            this.cmdPreviousSeason.Name = "cmdPreviousSeason";
            this.cmdPreviousSeason.Size = new System.Drawing.Size(115, 23);
            this.cmdPreviousSeason.TabIndex = 6;
            this.cmdPreviousSeason.Text = "Previous Season";
            this.cmdPreviousSeason.UseVisualStyleBackColor = true;
            this.cmdPreviousSeason.Click += new System.EventHandler(this.cmdPreviousSeason_Click);
            // 
            // cmdNextEpisode
            // 
            this.cmdNextEpisode.Location = new System.Drawing.Point(239, 134);
            this.cmdNextEpisode.Name = "cmdNextEpisode";
            this.cmdNextEpisode.Size = new System.Drawing.Size(115, 23);
            this.cmdNextEpisode.TabIndex = 7;
            this.cmdNextEpisode.Text = "Next Episode";
            this.cmdNextEpisode.UseVisualStyleBackColor = true;
            this.cmdNextEpisode.Click += new System.EventHandler(this.cmdNextEpisode_Click);
            // 
            // cmdNextSeason
            // 
            this.cmdNextSeason.Location = new System.Drawing.Point(239, 108);
            this.cmdNextSeason.Name = "cmdNextSeason";
            this.cmdNextSeason.Size = new System.Drawing.Size(115, 23);
            this.cmdNextSeason.TabIndex = 8;
            this.cmdNextSeason.Text = "Next Season";
            this.cmdNextSeason.UseVisualStyleBackColor = true;
            this.cmdNextSeason.Click += new System.EventHandler(this.cmdNextSeason_Click);
            // 
            // cmdPreviousEpisode
            // 
            this.cmdPreviousEpisode.Location = new System.Drawing.Point(118, 134);
            this.cmdPreviousEpisode.Name = "cmdPreviousEpisode";
            this.cmdPreviousEpisode.Size = new System.Drawing.Size(115, 23);
            this.cmdPreviousEpisode.TabIndex = 9;
            this.cmdPreviousEpisode.Text = "Previous Episode";
            this.cmdPreviousEpisode.UseVisualStyleBackColor = true;
            this.cmdPreviousEpisode.Click += new System.EventHandler(this.cmdPreviousEpisode_Click);
            // 
            // txtNameOnly
            // 
            this.txtNameOnly.Location = new System.Drawing.Point(12, 162);
            this.txtNameOnly.Name = "txtNameOnly";
            this.txtNameOnly.Size = new System.Drawing.Size(342, 20);
            this.txtNameOnly.TabIndex = 10;
            // 
            // txtTVDbName
            // 
            this.txtTVDbName.Location = new System.Drawing.Point(360, 162);
            this.txtTVDbName.Name = "txtTVDbName";
            this.txtTVDbName.Size = new System.Drawing.Size(328, 20);
            this.txtTVDbName.TabIndex = 11;
            // 
            // txtFullAddress
            // 
            this.txtFullAddress.Location = new System.Drawing.Point(12, 215);
            this.txtFullAddress.Name = "txtFullAddress";
            this.txtFullAddress.Size = new System.Drawing.Size(676, 20);
            this.txtFullAddress.TabIndex = 12;
            // 
            // cmdUseOwn
            // 
            this.cmdUseOwn.Location = new System.Drawing.Point(147, 186);
            this.cmdUseOwn.Name = "cmdUseOwn";
            this.cmdUseOwn.Size = new System.Drawing.Size(75, 23);
            this.cmdUseOwn.TabIndex = 13;
            this.cmdUseOwn.Text = "Use This";
            this.cmdUseOwn.UseVisualStyleBackColor = true;
            this.cmdUseOwn.Click += new System.EventHandler(this.cmdUseOwn_Click);
            // 
            // cmdUseTVDB
            // 
            this.cmdUseTVDB.Location = new System.Drawing.Point(482, 188);
            this.cmdUseTVDB.Name = "cmdUseTVDB";
            this.cmdUseTVDB.Size = new System.Drawing.Size(75, 23);
            this.cmdUseTVDB.TabIndex = 14;
            this.cmdUseTVDB.Text = "Use This";
            this.cmdUseTVDB.UseVisualStyleBackColor = true;
            this.cmdUseTVDB.Click += new System.EventHandler(this.cmdUseTVDB_Click);
            // 
            // txtFullAddressBefore
            // 
            this.txtFullAddressBefore.Location = new System.Drawing.Point(12, 241);
            this.txtFullAddressBefore.Name = "txtFullAddressBefore";
            this.txtFullAddressBefore.Size = new System.Drawing.Size(676, 20);
            this.txtFullAddressBefore.TabIndex = 15;
            // 
            // cmdSave
            // 
            this.cmdSave.Location = new System.Drawing.Point(12, 267);
            this.cmdSave.Name = "cmdSave";
            this.cmdSave.Size = new System.Drawing.Size(676, 23);
            this.cmdSave.TabIndex = 16;
            this.cmdSave.Text = "Save";
            this.cmdSave.UseVisualStyleBackColor = true;
            this.cmdSave.Click += new System.EventHandler(this.cmdSave_Click);
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(700, 399);
            this.Controls.Add(this.cmdSave);
            this.Controls.Add(this.txtFullAddressBefore);
            this.Controls.Add(this.cmdUseTVDB);
            this.Controls.Add(this.cmdUseOwn);
            this.Controls.Add(this.txtFullAddress);
            this.Controls.Add(this.txtTVDbName);
            this.Controls.Add(this.txtNameOnly);
            this.Controls.Add(this.cmdPreviousEpisode);
            this.Controls.Add(this.cmdNextSeason);
            this.Controls.Add(this.cmdNextEpisode);
            this.Controls.Add(this.cmdPreviousSeason);
            this.Controls.Add(this.txtEpisodeNum);
            this.Controls.Add(this.txtSeasonNum);
            this.Controls.Add(this.txtEpisode);
            this.Controls.Add(this.txtSeason);
            this.Controls.Add(this.cmdBrowse);
            this.Controls.Add(this.txtAddress);
            this.Name = "Form1";
            this.Text = "Form1";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.TextBox txtAddress;
        private System.Windows.Forms.Button cmdBrowse;
        private System.Windows.Forms.FolderBrowserDialog folderBrowserDialog1;
        private System.Windows.Forms.TextBox txtSeason;
        private System.Windows.Forms.TextBox txtEpisode;
        private System.Windows.Forms.TextBox txtSeasonNum;
        private System.Windows.Forms.TextBox txtEpisodeNum;
        private System.Windows.Forms.Button cmdPreviousSeason;
        private System.Windows.Forms.Button cmdNextEpisode;
        private System.Windows.Forms.Button cmdNextSeason;
        private System.Windows.Forms.Button cmdPreviousEpisode;
        private System.Windows.Forms.TextBox txtNameOnly;
        private System.Windows.Forms.TextBox txtTVDbName;
        private System.Windows.Forms.TextBox txtFullAddress;
        private System.Windows.Forms.Button cmdUseOwn;
        private System.Windows.Forms.Button cmdUseTVDB;
        private System.Windows.Forms.TextBox txtFullAddressBefore;
        private System.Windows.Forms.Button cmdSave;
    }
}

