﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Media_File_Renamer_Reloaded
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();

            myVariables.extensions = ".mp4|.avi|.mkv|.wmv|.flv|.mpg|.mov|.rmvb|.TS";
        }

        myVariables myVariables = new myVariables();
        extraMain extraMain = new extraMain();

        private void cmdBrowse_Click(object sender, EventArgs e)
        {
            myVariables.season = 0;
            myVariables.episode = 0;

            DialogResult folderResult = folderBrowserDialog1.ShowDialog();
            if (folderResult == DialogResult.OK)
            {
                string[] folderHolder = Directory.GetDirectories(folderBrowserDialog1.SelectedPath);
                extraMain.numericalSort(folderHolder);
                seriesSorter(folderHolder);

                episodeSorter();

                string[] seriesholder = folderBrowserDialog1.SelectedPath.Split('\\');
                myVariables.showName = seriesholder[seriesholder.Length - 1];

                txtAddress.Text = (folderBrowserDialog1.SelectedPath);
                txtSeason.Text = myVariables.folders[myVariables.season, 0];
                txtSeasonNum.Text = myVariables.folders[myVariables.season, 1];
                txtEpisode.Text = myVariables.files[myVariables.episode, 0];
                txtEpisodeNum.Text = myVariables.files[myVariables.episode, 1];
                txtNameOnly.Text = extraMain.replacer(myVariables.files[myVariables.episode, 0],myVariables.showName + "|" + myVariables.folders[myVariables.season, 0] + "|S" + myVariables.folders[myVariables.season, 1] + "|s" + myVariables.folders[myVariables.season, 1] + "|x" + myVariables.files[myVariables.episode, 1] + "|X" + myVariables.files[myVariables.episode, 1] + "|e" + myVariables.files[myVariables.episode, 1] + "|E" + myVariables.files[myVariables.episode, 1] + "| - ", "");
                txtFullAddressBefore.Text = myVariables.files[myVariables.episode, 2];

                string seriesName = extraMain.replacer(myVariables.showName, " ", "_");
                //myVariables.showXML = System.IO.File.ReadAllText(@"I:\Dev\" + seriesName + ".xml");

                string code = extraMain.pageDownloader("http://thetvdb.com/api/GetSeries.php?seriesname=" + seriesName, "<Series>", "</Series>");
                string seriesID = extraMain.xmlLoader(code, "<seriesid>", "</seriesid>");
                myVariables.showXML = extraMain.pageDownloader(("http://www.thetvdb.com/api/A2ABD84CB07FA632/series/" + seriesID + "/all"), "<Data>", "</Data>");

                tvdbNamer();

                nameUpdater();
            }
        }

        private void seriesSorter(string[] folderHolder)
        {
            myVariables.folders = new string[(folderHolder.Length), 2];

            try
            {
                for (int i = 0; i < folderHolder.Length; i++)
                {
                    string[] holder = folderHolder[i].Split('\\');
                    string[] holderNum = holder[holder.Length - 1].Split(' ');
                    string address = folderHolder[i];
                    string number = "00";

                    if (holderNum[holderNum.Length - 1] != "Specials")
                    {
                        if ((Convert.ToInt32(holderNum[holderNum.Length - 1])) > 9)
                        {
                            number = holderNum[holderNum.Length - 1];
                        }
                        else
                        {
                            number = "0" + holderNum[holderNum.Length - 1];
                        }
                    }
                    else
                    {
                        number = "00";
                    }

                    myVariables.folders[i, 0] = folderHolder[i];
                    myVariables.folders[i, 1] = number;
                }
            }
            catch
            {
                MessageBox.Show("something went wrong");
            }
        }

        private void episodeSorter()
        {
            string[] fileholder = extraMain.getFiles(myVariables.folders[myVariables.season, 0], myVariables.extensions, SearchOption.AllDirectories);

            myVariables.files = new string[(fileholder.Length), 3];
            Regex rgx = new Regex(@"(e|E|x|X)[\d][\d]");
            Regex rgx2 = new Regex(@"[\d][\d]");

            string episodeNum = "";

            for (int i = 0; i < fileholder.Length; i++)
            {
                int j = 0;
                foreach (Match match in rgx.Matches(fileholder[i]))
                {
                    if (j == 0)
                    {
                        string num = fileholder[i].Substring(match.Index - 1, 2);
                        episodeNum = (match.Value + "");
                        episodeNum = extraMain.replacer(episodeNum, "e|E|x|X", "");
                    }
                    j++;
                }
                myVariables.files[i, 0] = (extraMain.replacer(fileholder[i], myVariables.extensions + "|" + myVariables.folders[myVariables.season,0] + "|" + "\\", ""));
                myVariables.files[i, 1] = episodeNum;
                myVariables.files[i, 2] = fileholder[i];

            }
        }

        private void cmdPreviousSeason_Click(object sender, EventArgs e)
        {
            if (myVariables.season != 0)
            {
                myVariables.season--;
                myVariables.episode = 0;
                txtSeason.Text = myVariables.folders[myVariables.season, 0];
                txtSeasonNum.Text = myVariables.folders[myVariables.season, 1];
                episodeSorter();
                txtEpisode.Text = myVariables.files[myVariables.episode, 0];
                txtEpisodeNum.Text = myVariables.files[myVariables.episode, 1];
                txtNameOnly.Text = extraMain.replacer(myVariables.files[myVariables.episode, 0], myVariables.showName + "|" + myVariables.folders[myVariables.season, 0] + "|S" + myVariables.folders[myVariables.season, 1] + "|s" + myVariables.folders[myVariables.season, 1] + "|x" + myVariables.files[myVariables.episode, 1] + "|X" + myVariables.files[myVariables.episode, 1] + "|e" + myVariables.files[myVariables.episode, 1] + "|E" + myVariables.files[myVariables.episode, 1] + "| - ", "");
                txtFullAddressBefore.Text = myVariables.files[myVariables.episode, 2];
                tvdbNamer();
                nameUpdater();
            }
        }

        private void cmdNextSeason_Click(object sender, EventArgs e)
        {
            if (myVariables.season != (myVariables.folders.Length / 2) - 1)
            {
                myVariables.season++;
                myVariables.episode = 0;
                txtSeason.Text = myVariables.folders[myVariables.season, 0];
                txtSeasonNum.Text = myVariables.folders[myVariables.season, 1];
                episodeSorter();
                txtEpisode.Text = myVariables.files[myVariables.episode, 0];
                txtEpisodeNum.Text = myVariables.files[myVariables.episode, 1];
                txtNameOnly.Text = extraMain.replacer(myVariables.files[myVariables.episode, 0], myVariables.showName + "|" + myVariables.folders[myVariables.season, 0] + "|S" + myVariables.folders[myVariables.season, 1] + "|s" + myVariables.folders[myVariables.season, 1] + "|x" + myVariables.files[myVariables.episode, 1] + "|X" + myVariables.files[myVariables.episode, 1] + "|e" + myVariables.files[myVariables.episode, 1] + "|E" + myVariables.files[myVariables.episode, 1] + "| - ", "");
                txtFullAddressBefore.Text = myVariables.files[myVariables.episode, 2];
                tvdbNamer();
                nameUpdater();
            }
        }

        private void cmdPreviousEpisode_Click(object sender, EventArgs e)
        {
            if (myVariables.episode != 0)
            {
                myVariables.episode--;
                txtEpisode.Text = myVariables.files[myVariables.episode, 0];
                txtEpisodeNum.Text = myVariables.files[myVariables.episode, 1];
                txtNameOnly.Text = extraMain.replacer(myVariables.files[myVariables.episode, 0], myVariables.showName + "|" + myVariables.folders[myVariables.season, 0] + "|S" + myVariables.folders[myVariables.season, 1] + "|s" + myVariables.folders[myVariables.season, 1] + "|x" + myVariables.files[myVariables.episode, 1] + "|X" + myVariables.files[myVariables.episode, 1] + "|e" + myVariables.files[myVariables.episode, 1] + "|E" + myVariables.files[myVariables.episode, 1] + "| - ", "");
                txtFullAddressBefore.Text = myVariables.files[myVariables.episode, 2];
                tvdbNamer();
                nameUpdater();
            }
        }

        private void cmdNextEpisode_Click(object sender, EventArgs e)
        {
            if (myVariables.episode != (myVariables.files.Length / 3) - 1)
            {
                myVariables.episode++;
                txtEpisode.Text = myVariables.files[myVariables.episode, 0];
                txtEpisodeNum.Text = myVariables.files[myVariables.episode, 1];
                txtNameOnly.Text = extraMain.replacer(myVariables.files[myVariables.episode, 0], myVariables.showName + "|" + myVariables.folders[myVariables.season, 0] + "|S" + myVariables.folders[myVariables.season, 1] + "|s" + myVariables.folders[myVariables.season, 1] + "|x" + myVariables.files[myVariables.episode, 1] + "|X" + myVariables.files[myVariables.episode, 1] + "|e" + myVariables.files[myVariables.episode, 1] + "|E" + myVariables.files[myVariables.episode, 1] + "| - ", "");
                txtFullAddressBefore.Text = myVariables.files[myVariables.episode, 2];
                tvdbNamer();
                nameUpdater();
            }
        }

        private void tvdbNamer()
        {
            string seasonNum = txtSeasonNum.Text;
            string episodeNum = txtEpisodeNum.Text;

            if (Convert.ToInt32(seasonNum) < 10 && Convert.ToInt32(seasonNum) != 00)
            {
                seasonNum = seasonNum.Replace("0", "");
            }
            else if (Convert.ToInt32(seasonNum) == 00)
            {
                seasonNum = "0";
            }


            if (Convert.ToInt32(episodeNum) < 10)
            {
                episodeNum = episodeNum.Replace("0", "");
            }

            //string xml = extraMain.xmlLoader(myVariables.showXML, "<Data>", "</Data>");

            string[] episodes = myVariables.showXML.Split(new string[] { "<Episode>" }, StringSplitOptions.None);

            string name = "";

            foreach (string episode in episodes)
            {
                if (episode.Contains("<EpisodeNumber>" + episodeNum + "</EpisodeNumber>") && episode.Contains("<SeasonNumber>" + seasonNum + "</SeasonNumber>"))
                {
                    name = extraMain.xmlLoader(episode, "<EpisodeName>", "</EpisodeName>");
                }
            }

            if (name != null)
            {
                name = extraMain.replacer(name, "\\|/|?|:|*|\"|>|<", "");
            }

            txtTVDbName.Text = name;
        }

        private void nameUpdater()
        {
           txtFullAddress.Text = txtSeason.Text + "\\" + myVariables.showName + " - s" + txtSeasonNum.Text + "e" + txtEpisodeNum.Text + " - " + txtNameOnly.Text;
        }

        private void cmdUseOwn_Click(object sender, EventArgs e)
        {
            txtFullAddress.Text = txtSeason.Text + "\\" + myVariables.showName + " - s" + txtSeasonNum.Text + "e" + txtEpisodeNum.Text + " - " + txtNameOnly.Text;
        }

        private void cmdUseTVDB_Click(object sender, EventArgs e)
        {
            txtFullAddress.Text = txtSeason.Text + "\\" + myVariables.showName + " - s" + txtSeasonNum.Text + "e" + txtEpisodeNum.Text + " - " + txtTVDbName.Text;
        }

        private void cmdSave_Click(object sender, EventArgs e)
        {
            try
            {
                FileInfo file = new FileInfo(@txtFullAddress.Text);

                string[] MultipleFilters = myVariables.extensions.Split('|');

                foreach (string extension in MultipleFilters)
                {
                    if (myVariables.files[myVariables.episode, 2].EndsWith(extension))
                    {
                        File.Move(@myVariables.files[myVariables.episode, 2], file + extension);
                    }
                }

                if (myVariables.episode != (myVariables.files.Length / 3) - 1)
                {
                    myVariables.episode++;
                    txtEpisode.Text = myVariables.files[myVariables.episode, 0];
                    txtEpisodeNum.Text = myVariables.files[myVariables.episode, 1];
                    txtNameOnly.Text = extraMain.replacer(myVariables.files[myVariables.episode, 0], myVariables.showName + "|" + myVariables.folders[myVariables.season, 0] + "|S" + myVariables.folders[myVariables.season, 1] + "|s" + myVariables.folders[myVariables.season, 1] + "|x" + myVariables.files[myVariables.episode, 1] + "|X" + myVariables.files[myVariables.episode, 1] + "|e" + myVariables.files[myVariables.episode, 1] + "|E" + myVariables.files[myVariables.episode, 1] + "| - ", "");
                    txtFullAddressBefore.Text = myVariables.files[myVariables.episode, 2];
                    tvdbNamer();
                    nameUpdater();
                }
            }
            catch (FileNotFoundException)
            {
                MessageBox.Show("File Not Found!");
            }
            catch (ArgumentException)
            {
                MessageBox.Show("Error in FileName!");
            }
            catch (NotSupportedException)
            {
                MessageBox.Show("Error in FileName!");
            }
        }
    }

    public class myVariables
    {
        public string extensions { get; set; }
        public string showName { get; set; }
        public string showXML { get; set; }
        public string[,] folders { get; set; }
        public string[,] files { get; set; }
        public int season { get; set; }
        public int episode { get; set; }
    }
}
