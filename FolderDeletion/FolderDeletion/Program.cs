﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FolderDeletion
{
    class Program
    {

        extraMain extraMain = new extraMain();

        static void Main(string[] args)
        {
            new Program().main2(args);
        }

        private void main2(string[] args)
        {
            string extensions = ".mp4|.avi|.mkv|.wmv|.flv|.mpg|.mov|.rmvb|.TS";

            string[] seasons = Directory.GetDirectories(args[0]);

            StreamReader reader = new StreamReader(@"ignore.txt");
            string listerTmp = reader.ReadToEnd();
            string[] multiIgnore = listerTmp.Split('|');

            for (int i = 0; i < seasons.Length; i++) //Cleans the show names
            {
                seasons[i] = extraMain.replacer(seasons[i], args[0] + "|" + "\\", "");
                //seasons[i] = extraMain.replacer(seasons[i], " ", "_");
                //Console.WriteLine(seasons[i]);
            }

            foreach (string season in seasons)
            {
                bool ignored = false;
                foreach (string ignore in multiIgnore)
                {
                    if (season == ignore && ignored == false) // Checks if show is on ignore list
                    {
                        ignored = true;
                    }
                }

                if (ignored == false)
                {
                    string[] folderHolder = Directory.GetDirectories(args[0] + "\\" + season);

                    foreach(string folder in folderHolder)
                    {
                        

                        Console.WriteLine(folder);

                        string[] folderHolderMove = Directory.GetDirectories(folder);

                        foreach(string folderMove in folderHolderMove)
                        {
                            //Console.WriteLine(folderMove);
                            string[] fileHolder = extraMain.getFiles(folderMove, extensions, SearchOption.AllDirectories);

                            foreach (string filei in fileHolder)
                            {
                                //Console.WriteLine(file);

                                try
                                {
                                    string temp = extraMain.replacer(filei, folderMove + "|\\|//", "");
                                    string temp2 = filei.Replace(temp, "");

                                    Console.WriteLine(temp2);
                                    Console.WriteLine(filei);

                                    File.Move(filei, folder + "\\" + temp);
                                    Directory.Delete(temp2, true);

                                }
                                catch (FileNotFoundException)
                                {

                                }
                                catch (ArgumentException)
                                {

                                }
                                catch (NotSupportedException)
                                {

                                }
                                catch (IOException)
                                {

                                }
                                Console.ReadLine();
                            }

                        }
                    }

                    
                }
            }

            Console.ReadLine();
        }
    }
}
