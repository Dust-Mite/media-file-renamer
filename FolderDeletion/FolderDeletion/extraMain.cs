﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using System.Xml;

//Created by Matthew Moore
//Email: matthewemmoore@googlemail.com
//Originally created on 15/10/2013
//Last edited: 16/10/2013

    class extraMain
    {
        public string replacer(string input, string replace, string replacement)
        {
            string[] multiReplace = replace.Split('|');

            foreach (string replacer in multiReplace)
            {
                input = input.Replace(replacer, replacement);
            }

            return input;
        }

        public string[] getFiles(string sourceFolder, string filter, System.IO.SearchOption searchOption)
        {
            ArrayList alFiles = new ArrayList();

            string[] multipleFilters = filter.Split('|');

            string[] holder = Directory.GetFiles(sourceFolder);

            foreach (string file in holder)
            {
                foreach (string extension in multipleFilters)
                {
                    if (file.EndsWith(extension))
                    {
                        alFiles.Add(file);
                    }
                }
            }

            return (string[])alFiles.ToArray(typeof(string));
        }

        public static void numericalSort(string[] ar)
        {
            Regex rgx = new Regex("([^0-9]*)([0-9]+)");
            Array.Sort(ar, (a, b) =>
            {
                var ma = rgx.Matches(a);
                var mb = rgx.Matches(b);
                for (int i = 0; i < ma.Count; ++i)
                {
                    int ret = ma[i].Groups[1].Value.CompareTo(mb[i].Groups[1].Value);
                    if (ret != 0)
                        return ret;

                    ret = int.Parse(ma[i].Groups[2].Value) - int.Parse(mb[i].Groups[2].Value);
                    if (ret != 0)
                        return ret;
                }

                return 0;
            });
        }

        public string xmlLoader(string tmpList, string cutBegin, string cutEnd)
        {
            int Listi = 0;
            int Listj = 0;
            int Run = 0;
            string cutString = "";

            while ((Listi = tmpList.IndexOf(cutBegin, Listi)) != -1)
            {
                if (Run == 0)
                {
                    Listj = tmpList.IndexOf(cutEnd, Listi);
                    cutString = tmpList.Substring(Listi, (Listj - Listi));
                    cutString = cutString.Replace(cutBegin, "");
                    Run++;
                }
                Listi++;
            }
            return cutString;
        }

        public void xmlWriter(string file, string xmlNode, string element, string data)
        {
            XmlDocument document = new XmlDocument();
            document.Load(@file);

            XmlNode node = document.CreateNode(XmlNodeType.Element, xmlNode, null);

            XmlNode nodeXML = document.CreateElement(element);

            nodeXML.InnerText = data;

            node.AppendChild(nodeXML);

            document.DocumentElement.AppendChild(node);

            document.Save(file);
        }

        public string pageDownloader(string urlS, string first, string second)
        {
            string cutOutput = "";
            Uri url = new Uri(urlS);
            if (url.Scheme == Uri.UriSchemeHttp)
            {
                HttpWebRequest objRequest = (HttpWebRequest)HttpWebRequest.Create(url);
                objRequest.Method = WebRequestMethods.Http.Get;
                HttpWebResponse objResponse = (HttpWebResponse)objRequest.GetResponse();
                StreamReader reader = new StreamReader(objResponse.GetResponseStream());
                string tmp = reader.ReadToEnd();
                int firstI = tmp.IndexOf(first);
                int secondI = tmp.IndexOf(second);
                cutOutput = tmp.Substring(firstI, (secondI - firstI));
            }
            return cutOutput;
        }
    }
