﻿namespace Media_File_Renamer
{
    partial class Form1
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.folderBrowserDialog1 = new System.Windows.Forms.FolderBrowserDialog();
            this.txtFolderBrowser = new System.Windows.Forms.TextBox();
            this.cmdBrowse = new System.Windows.Forms.Button();
            this.txtSeason = new System.Windows.Forms.TextBox();
            this.cmdNextSeason = new System.Windows.Forms.Button();
            this.cmdPreviousSeason = new System.Windows.Forms.Button();
            this.txtEpisode = new System.Windows.Forms.TextBox();
            this.cmdPreviousEpisode = new System.Windows.Forms.Button();
            this.cmdNextEpisode = new System.Windows.Forms.Button();
            this.txtSeasonNameSet = new System.Windows.Forms.TextBox();
            this.txtSeasonSet = new System.Windows.Forms.TextBox();
            this.txtEpisodeSet = new System.Windows.Forms.TextBox();
            this.txtNameSet = new System.Windows.Forms.TextBox();
            this.txtUpdater = new System.Windows.Forms.TextBox();
            this.cmdUpdate = new System.Windows.Forms.Button();
            this.cmdSave = new System.Windows.Forms.Button();
            this.txtDirectory = new System.Windows.Forms.TextBox();
            this.txtTVDBName = new System.Windows.Forms.TextBox();
            this.cmdTVDB = new System.Windows.Forms.Button();
            this.txtOriginalName = new System.Windows.Forms.TextBox();
            this.SuspendLayout();
            // 
            // folderBrowserDialog1
            // 
            this.folderBrowserDialog1.RootFolder = System.Environment.SpecialFolder.MyComputer;
            this.folderBrowserDialog1.ShowNewFolderButton = false;
            // 
            // txtFolderBrowser
            // 
            this.txtFolderBrowser.Location = new System.Drawing.Point(12, 12);
            this.txtFolderBrowser.Name = "txtFolderBrowser";
            this.txtFolderBrowser.Size = new System.Drawing.Size(541, 20);
            this.txtFolderBrowser.TabIndex = 0;
            // 
            // cmdBrowse
            // 
            this.cmdBrowse.Location = new System.Drawing.Point(559, 12);
            this.cmdBrowse.Name = "cmdBrowse";
            this.cmdBrowse.Size = new System.Drawing.Size(120, 23);
            this.cmdBrowse.TabIndex = 1;
            this.cmdBrowse.Text = "Browse";
            this.cmdBrowse.UseVisualStyleBackColor = true;
            this.cmdBrowse.Click += new System.EventHandler(this.button1_Click);
            // 
            // txtSeason
            // 
            this.txtSeason.Enabled = false;
            this.txtSeason.Location = new System.Drawing.Point(12, 40);
            this.txtSeason.Name = "txtSeason";
            this.txtSeason.Size = new System.Drawing.Size(327, 20);
            this.txtSeason.TabIndex = 2;
            // 
            // cmdNextSeason
            // 
            this.cmdNextSeason.Enabled = false;
            this.cmdNextSeason.Location = new System.Drawing.Point(452, 38);
            this.cmdNextSeason.Name = "cmdNextSeason";
            this.cmdNextSeason.Size = new System.Drawing.Size(101, 23);
            this.cmdNextSeason.TabIndex = 3;
            this.cmdNextSeason.Text = "Next Season";
            this.cmdNextSeason.UseVisualStyleBackColor = true;
            this.cmdNextSeason.Click += new System.EventHandler(this.cmdNextSeason_Click);
            // 
            // cmdPreviousSeason
            // 
            this.cmdPreviousSeason.Enabled = false;
            this.cmdPreviousSeason.Location = new System.Drawing.Point(345, 38);
            this.cmdPreviousSeason.Name = "cmdPreviousSeason";
            this.cmdPreviousSeason.Size = new System.Drawing.Size(101, 23);
            this.cmdPreviousSeason.TabIndex = 4;
            this.cmdPreviousSeason.Text = "Previous Season";
            this.cmdPreviousSeason.UseVisualStyleBackColor = true;
            this.cmdPreviousSeason.Click += new System.EventHandler(this.cmdPreviousSeason_Click);
            // 
            // txtEpisode
            // 
            this.txtEpisode.Enabled = false;
            this.txtEpisode.Location = new System.Drawing.Point(12, 66);
            this.txtEpisode.Name = "txtEpisode";
            this.txtEpisode.Size = new System.Drawing.Size(327, 20);
            this.txtEpisode.TabIndex = 5;
            // 
            // cmdPreviousEpisode
            // 
            this.cmdPreviousEpisode.Enabled = false;
            this.cmdPreviousEpisode.Location = new System.Drawing.Point(345, 64);
            this.cmdPreviousEpisode.Name = "cmdPreviousEpisode";
            this.cmdPreviousEpisode.Size = new System.Drawing.Size(101, 23);
            this.cmdPreviousEpisode.TabIndex = 6;
            this.cmdPreviousEpisode.Text = "Previous Episode";
            this.cmdPreviousEpisode.UseVisualStyleBackColor = true;
            this.cmdPreviousEpisode.Click += new System.EventHandler(this.button1_Click_1);
            // 
            // cmdNextEpisode
            // 
            this.cmdNextEpisode.Enabled = false;
            this.cmdNextEpisode.Location = new System.Drawing.Point(452, 64);
            this.cmdNextEpisode.Name = "cmdNextEpisode";
            this.cmdNextEpisode.Size = new System.Drawing.Size(101, 23);
            this.cmdNextEpisode.TabIndex = 7;
            this.cmdNextEpisode.Text = "Next Episode";
            this.cmdNextEpisode.UseVisualStyleBackColor = true;
            this.cmdNextEpisode.Click += new System.EventHandler(this.cmdNextEpisode_Click);
            // 
            // txtSeasonNameSet
            // 
            this.txtSeasonNameSet.Enabled = false;
            this.txtSeasonNameSet.Location = new System.Drawing.Point(12, 156);
            this.txtSeasonNameSet.Name = "txtSeasonNameSet";
            this.txtSeasonNameSet.Size = new System.Drawing.Size(327, 20);
            this.txtSeasonNameSet.TabIndex = 8;
            // 
            // txtSeasonSet
            // 
            this.txtSeasonSet.Enabled = false;
            this.txtSeasonSet.Location = new System.Drawing.Point(12, 182);
            this.txtSeasonSet.Name = "txtSeasonSet";
            this.txtSeasonSet.Size = new System.Drawing.Size(100, 20);
            this.txtSeasonSet.TabIndex = 9;
            // 
            // txtEpisodeSet
            // 
            this.txtEpisodeSet.Enabled = false;
            this.txtEpisodeSet.Location = new System.Drawing.Point(118, 182);
            this.txtEpisodeSet.Name = "txtEpisodeSet";
            this.txtEpisodeSet.Size = new System.Drawing.Size(100, 20);
            this.txtEpisodeSet.TabIndex = 10;
            // 
            // txtNameSet
            // 
            this.txtNameSet.Enabled = false;
            this.txtNameSet.Location = new System.Drawing.Point(12, 208);
            this.txtNameSet.Name = "txtNameSet";
            this.txtNameSet.Size = new System.Drawing.Size(327, 20);
            this.txtNameSet.TabIndex = 11;
            // 
            // txtUpdater
            // 
            this.txtUpdater.Enabled = false;
            this.txtUpdater.Location = new System.Drawing.Point(12, 266);
            this.txtUpdater.Name = "txtUpdater";
            this.txtUpdater.Size = new System.Drawing.Size(541, 20);
            this.txtUpdater.TabIndex = 12;
            // 
            // cmdUpdate
            // 
            this.cmdUpdate.Enabled = false;
            this.cmdUpdate.Location = new System.Drawing.Point(559, 264);
            this.cmdUpdate.Name = "cmdUpdate";
            this.cmdUpdate.Size = new System.Drawing.Size(120, 23);
            this.cmdUpdate.TabIndex = 13;
            this.cmdUpdate.Text = "Update";
            this.cmdUpdate.UseVisualStyleBackColor = true;
            this.cmdUpdate.Click += new System.EventHandler(this.cmdUpdate_Click);
            // 
            // cmdSave
            // 
            this.cmdSave.Enabled = false;
            this.cmdSave.Location = new System.Drawing.Point(12, 292);
            this.cmdSave.Name = "cmdSave";
            this.cmdSave.Size = new System.Drawing.Size(667, 23);
            this.cmdSave.TabIndex = 14;
            this.cmdSave.Text = "Save";
            this.cmdSave.UseVisualStyleBackColor = true;
            this.cmdSave.Click += new System.EventHandler(this.cmdSave_Click);
            // 
            // txtDirectory
            // 
            this.txtDirectory.Enabled = false;
            this.txtDirectory.Location = new System.Drawing.Point(12, 130);
            this.txtDirectory.Name = "txtDirectory";
            this.txtDirectory.Size = new System.Drawing.Size(327, 20);
            this.txtDirectory.TabIndex = 15;
            // 
            // txtTVDBName
            // 
            this.txtTVDBName.Location = new System.Drawing.Point(346, 208);
            this.txtTVDBName.Name = "txtTVDBName";
            this.txtTVDBName.Size = new System.Drawing.Size(207, 20);
            this.txtTVDBName.TabIndex = 16;
            // 
            // cmdTVDB
            // 
            this.cmdTVDB.Location = new System.Drawing.Point(559, 206);
            this.cmdTVDB.Name = "cmdTVDB";
            this.cmdTVDB.Size = new System.Drawing.Size(120, 23);
            this.cmdTVDB.TabIndex = 17;
            this.cmdTVDB.Text = "TVDB Name";
            this.cmdTVDB.UseVisualStyleBackColor = true;
            this.cmdTVDB.Click += new System.EventHandler(this.cmdTVDB_Click);
            // 
            // txtOriginalName
            // 
            this.txtOriginalName.Location = new System.Drawing.Point(12, 104);
            this.txtOriginalName.Name = "txtOriginalName";
            this.txtOriginalName.Size = new System.Drawing.Size(541, 20);
            this.txtOriginalName.TabIndex = 18;
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(691, 356);
            this.Controls.Add(this.txtOriginalName);
            this.Controls.Add(this.cmdTVDB);
            this.Controls.Add(this.txtTVDBName);
            this.Controls.Add(this.txtDirectory);
            this.Controls.Add(this.cmdSave);
            this.Controls.Add(this.cmdUpdate);
            this.Controls.Add(this.txtUpdater);
            this.Controls.Add(this.txtNameSet);
            this.Controls.Add(this.txtEpisodeSet);
            this.Controls.Add(this.txtSeasonSet);
            this.Controls.Add(this.txtSeasonNameSet);
            this.Controls.Add(this.cmdNextEpisode);
            this.Controls.Add(this.cmdPreviousEpisode);
            this.Controls.Add(this.txtEpisode);
            this.Controls.Add(this.cmdPreviousSeason);
            this.Controls.Add(this.cmdNextSeason);
            this.Controls.Add(this.txtSeason);
            this.Controls.Add(this.cmdBrowse);
            this.Controls.Add(this.txtFolderBrowser);
            this.Name = "Form1";
            this.Text = "Form1";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.FolderBrowserDialog folderBrowserDialog1;
        private System.Windows.Forms.TextBox txtFolderBrowser;
        private System.Windows.Forms.Button cmdBrowse;
        private System.Windows.Forms.TextBox txtSeason;
        private System.Windows.Forms.Button cmdNextSeason;
        private System.Windows.Forms.Button cmdPreviousSeason;
        private System.Windows.Forms.TextBox txtEpisode;
        private System.Windows.Forms.Button cmdPreviousEpisode;
        private System.Windows.Forms.Button cmdNextEpisode;
        private System.Windows.Forms.TextBox txtSeasonNameSet;
        private System.Windows.Forms.TextBox txtSeasonSet;
        private System.Windows.Forms.TextBox txtEpisodeSet;
        private System.Windows.Forms.TextBox txtNameSet;
        private System.Windows.Forms.TextBox txtUpdater;
        private System.Windows.Forms.Button cmdUpdate;
        private System.Windows.Forms.Button cmdSave;
        private System.Windows.Forms.TextBox txtDirectory;
        private System.Windows.Forms.TextBox txtTVDBName;
        private System.Windows.Forms.Button cmdTVDB;
        private System.Windows.Forms.TextBox txtOriginalName;
    }
}

