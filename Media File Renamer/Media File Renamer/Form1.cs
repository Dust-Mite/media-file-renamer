﻿using System;
using System.IO;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Collections;
using System.Text.RegularExpressions;

namespace Media_File_Renamer
{

    public partial class Form1 : Form
    {

        public Form1()
        {
            InitializeComponent();
        }

        myVariables myVariables = new myVariables();
        extraMain extraMain = new extraMain();

        public void button1_Click(object sender, EventArgs e)
        {

            DialogResult folderResult = folderBrowserDialog1.ShowDialog();
            if (folderResult == DialogResult.OK)
            {
                myVariables.extensions = ".mp4|.avi|.mkv|.wmv|.flv|.mpg|.mov|.rmvb";

                myVariables.subFolders = Directory.GetDirectories(folderBrowserDialog1.SelectedPath);
                extraMain.numericalSort(myVariables.subFolders);
                myVariables.showName = folderBrowserDialog1.SelectedPath;
                txtFolderBrowser.Text = (folderBrowserDialog1.SelectedPath);

                string[] seasonName = folderBrowserDialog1.SelectedPath.Split('\\');

                txtSeasonNameSet.Text = (seasonName[seasonName.Length - 1]);

                txtSeason.Enabled = true;
                txtEpisode.Enabled = true;
                txtDirectory.Enabled = true;
                txtSeasonNameSet.Enabled = true;
                txtSeasonSet.Enabled = true;
                txtEpisodeSet.Enabled = true;
                txtNameSet.Enabled = true;
                txtUpdater.Enabled = true;
                cmdNextSeason.Enabled = true;
                cmdPreviousSeason.Enabled = true;
                cmdPreviousEpisode.Enabled = true;
                cmdNextEpisode.Enabled = true;
                cmdUpdate.Enabled = true;
                cmdSave.Enabled = true;

                myVariables.seasonNum = 1;

                changeSeason();
            }
        }

        public void cmdPreviousSeason_Click(object sender, EventArgs e)
        {

            if (myVariables.seasonNum != 1)
            {
                myVariables.seasonNum--;
                changeSeason();
            }

        }

        private void cmdNextSeason_Click(object sender, EventArgs e)
        {  

            if (myVariables.seasonNum != myVariables.subFolders.Length)
            {
                myVariables.seasonNum++;
                changeSeason();
            }
        }

        private void changeSeason()
        {
            string seasonNum = "";
            string seasonName = myVariables.subFolders[(myVariables.seasonNum - 1)];
            seasonName = seasonName.Replace(myVariables.showName + @"\", "");
            txtSeason.Text = seasonName;

            myVariables.files = extraMain.getFiles(myVariables.subFolders[myVariables.seasonNum - 1], myVariables.extensions, SearchOption.AllDirectories);

            myVariables.episodeNum = 1;
            txtEpisode.Text = episodeCleaner();

            string[] directoryName = myVariables.subFolders[myVariables.seasonNum - 1].Split('\\');

            string[] directoryNum = directoryName[directoryName.Length - 1].Split(' ');

            if (directoryNum[directoryNum.Length - 1] != "Specials")
            {

                if ((Convert.ToInt32(directoryNum[directoryNum.Length - 1])) > 9)
                {
                    seasonNum = "s" + directoryNum[directoryNum.Length - 1];
                }
                else
                {
                    seasonNum = "s0" + directoryNum[directoryNum.Length - 1];
                }
            }
            else
            {
                seasonNum = "s00";
            }
            txtSeasonSet.Text = seasonNum;

            episodeSaver();
        }

        private string episodeCleaner()
        {
            string episodeName = myVariables.files[myVariables.episodeNum - 1];

            episodeName = extraMain.replacer(episodeName, myVariables.extensions + "| - |" + myVariables.subFolders[myVariables.seasonNum - 1] + @"\", "");

            string[] directoryName = myVariables.subFolders[myVariables.seasonNum - 1].Split('\\');

            string[] directoryNum = directoryName[directoryName.Length - 1].Split(' ');

            if (directoryNum[directoryNum.Length - 1] != "Specials")
            {

                if (myVariables.episodeNum > 9)
                {
                    episodeName = extraMain.replacer(episodeName, String.Format("e{0}|E{0}| {0}", myVariables.episodeNum), "");
                }
                else
                {
                    episodeName = extraMain.replacer(episodeName, String.Format("e0{0}|E0{0}|0{0}", myVariables.episodeNum), "");
                }

                if (myVariables.seasonNum > 9)
                {
                    episodeName = extraMain.replacer(episodeName, String.Format("s{0}|S{0}|{0}x|{0}X", directoryNum[directoryNum.Length - 1]), "");
                }
                else
                {
                    episodeName = extraMain.replacer(episodeName, String.Format("s0{0}|S0{0}|0{0}|{0}x|{0}X", directoryNum[directoryNum.Length - 1]), "");
                }
            }
            else
            {
                    episodeName = extraMain.replacer(episodeName, String.Format("s{0}|S{0}|{0}x|{0}X", "00"), "");
            }

            txtEpisode.Text = episodeName;
            return episodeName;
        }

        private void button1_Click_1(object sender, EventArgs e)
        {
            if (myVariables.episodeNum != 1)
            {
                myVariables.episodeNum--;
                txtEpisode.Text = episodeCleaner();

                episodeSaver();
            }
        }

        private void cmdNextEpisode_Click(object sender, EventArgs e)
        {
            if (myVariables.episodeNum != myVariables.files.Length)
            {
                myVariables.episodeNum++;
                txtEpisode.Text = episodeCleaner();

                episodeSaver();
            }
        }

        private void episodeSaver()
        {
            string seasonName = txtSeasonNameSet.Text;
            string episodeNum = myVariables.episodeNum + "";
            string episodeName = episodeCleaner();
            string directory = myVariables.subFolders[myVariables.seasonNum - 1];

            string[] directoryName = directory.Split('\\');

            string[] directoryNum = directoryName[directoryName.Length - 1].Split(' ');

            if (myVariables.episodeNum > 9 )
            {
                episodeNum = "e" + myVariables.episodeNum;
            }
            else
            {
                episodeNum = "e0" + myVariables.episodeNum;
            }

            episodeName = extraMain.replacer(episodeName, seasonName + " |" + seasonName, "");

            //txtDirectory.Text = directoryNum[directoryNum.Length - 1];
            txtOriginalName.Text = myVariables.files[myVariables.episodeNum - 1];
            txtDirectory.Text = myVariables.subFolders[myVariables.seasonNum - 1];
            txtEpisodeSet.Text = episodeNum;
            txtNameSet.Text = episodeName;

            //Regex rx = new Regex(@"(e|x)[\d][\d]", RegexOptions.Compiled | RegexOptions.IgnoreCase);

            //if (rx.IsMatch(episodeName))
            //{
            //    MessageBox.Show("Test");
            //}

            //MatchCollection matches = rx.Matches(txtNameSet.Text);

            //foreach (Match match in matches)
            //{
            //    GroupCollection groups = match.Groups;
            //    MessageBox.Show(groups["word"].Value);
            //}

            updater();

        }

        private void updater()
        {
            txtUpdater.Text = txtDirectory.Text + @"\" + txtSeasonNameSet.Text + " - " + txtSeasonSet.Text + txtEpisodeSet.Text + " - " + txtNameSet.Text;
        }

        private void cmdUpdate_Click(object sender, EventArgs e)
        {
            updater();
        }

        private void cmdSave_Click(object sender, EventArgs e)
        {
            try
            {
                FileInfo file = new FileInfo(@txtUpdater.Text);

                string[] MultipleFilters = myVariables.extensions.Split('|');

                foreach (string extension in MultipleFilters)
                {
                    if (myVariables.files[myVariables.episodeNum - 1].EndsWith(extension))
                    {
                        File.Move(@myVariables.files[myVariables.episodeNum - 1], file + extension);
                    }
                }

                if (myVariables.episodeNum != myVariables.files.Length)
                {
                    myVariables.episodeNum++;
                    txtEpisode.Text = episodeCleaner();

                    episodeSaver();
                }
            }
            catch (FileNotFoundException)
            {
                MessageBox.Show("File Not Found!");
            }
            catch (ArgumentException)
            {
                MessageBox.Show("Error in FileName!");
            }
            catch (NotSupportedException)
            {
                MessageBox.Show("Error in FileName!");
            }
        }

        private string tvdbNamer(string seriesName, string seasonNum, string episodeNum)
        {
            string seriesName_ = extraMain.replacer(seriesName, " ", "_");

            string holder = System.IO.File.ReadAllText(@"I:\Dev\" + seriesName_ + ".xml");

            string xml = extraMain.xmlLoader(holder, "<Data>", "</Data>");

            string[] episodes = xml.Split(new string[] { "<Episode>" }, StringSplitOptions.None);

            string name = "";

            foreach (string episode in episodes)
            {
                if (episode.Contains("<EpisodeNumber>" + episodeNum + "</EpisodeNumber>") && episode.Contains("<SeasonNumber>" + seasonNum + "</SeasonNumber>"))
                {
                    name = extraMain.xmlLoader(episode, "<EpisodeName>", "</EpisodeName>");
                }

            }
            return (name);
        }

        private void cmdTVDB_Click(object sender, EventArgs e)
        {
            if ((Convert.ToInt32(txtSeasonSet.Text.Replace("s", "")) < 10) && (Convert.ToInt32(txtEpisodeSet.Text.Replace("e", "")) < 10))
            {
                txtTVDBName.Text = tvdbNamer(txtSeasonNameSet.Text, (txtSeasonSet.Text.Replace("s0", "")), (txtEpisodeSet.Text.Replace("e0", "")));
            }

            if ((Convert.ToInt32(txtSeasonSet.Text.Replace("s", "")) < 10) && (Convert.ToInt32(txtEpisodeSet.Text.Replace("e", "")) > 10))
            {
                txtTVDBName.Text = tvdbNamer(txtSeasonNameSet.Text, (txtSeasonSet.Text.Replace("s0", "")), (txtEpisodeSet.Text.Replace("e", "")));
            }

            if ((Convert.ToInt32(txtSeasonSet.Text.Replace("s", "")) > 10) && (Convert.ToInt32(txtEpisodeSet.Text.Replace("e", "")) < 10))
            {
                txtTVDBName.Text = tvdbNamer(txtSeasonNameSet.Text, (txtSeasonSet.Text.Replace("s", "")), (txtEpisodeSet.Text.Replace("e0", "")));
            }

            if ((Convert.ToInt32(txtSeasonSet.Text.Replace("s", "")) > 10) && (Convert.ToInt32(txtEpisodeSet.Text.Replace("e", "")) > 10))
            {
                txtTVDBName.Text = tvdbNamer(txtSeasonNameSet.Text, (txtSeasonSet.Text.Replace("s", "")), (txtEpisodeSet.Text.Replace("e", "")));
            }
        }

    }

    public class myVariables
    {

        public string[] subFolders { get; set; }
        public string[] seasonLength { get; set; }
        public string[] files { get; set; }
        public int seasonNum { get; set; }
        public int episodeNum { get; set; }
        public string showName { get; set; }
        public string extensions { get; set; }

    }

}
